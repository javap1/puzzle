package controller;

/**
 * @author Mohamed Akaddar
 * @date 18 mars 2022
 * AbstractTaquinListenable
 */


public interface AbstractTaquinListenable{
  /**
  * methode pour ajouter un listener à la liste des listeners
  * @param listener Listener à ajouter
  */
  public void ajouterTaquinListener(TaquinListener listener);

  /**
  * methode pour supprimer un listener dans la liste des listeners
  * @param listener Listener à ajouter
  */
  public void supprimerTaquinListener(TaquinListener listener);

  /**
  * methode qui informe si un déplacement a été fait
  */
  public void infoEstDeplace();
}
