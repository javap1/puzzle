
package controller;

/**
 * @author Mohamed Akaddar
 * @date 18 mars 2022
 * AbstractTaquinListenable
 */

public interface TaquinListener {

/**
* elle sera appelée après chaque déplacement
*/
   public void move();
}
