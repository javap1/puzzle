package model;
import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import javax.swing.*;

import controller.AbstractTaquinListenable;
import controller.TaquinListener;
import vue.vueGraphique.AbstracTaquin;


public class Taquin implements AbstractTaquinListenable{

  /*  @author Larbi ben fares  */

  
     private Integer[][] board;
	   private Integer[][] etatInitiale;
     private int nligne;
     private int nColonne;
     private int[] crdVide = new int[2];

     private List<TaquinListener> tqListeners;
     private boolean informer;
      
 public Integer[][] CopyBoard(){
   Integer[][] d=new Integer[this.nColonne][this.nligne];
   for(int i=0;i<this.nligne;i++)
	    for(int j=0;j<this.nColonne;j++)
               d[j][i]=this.board[j][i];
   return d;


}
 public Taquin(int m,int n)
 {  this.nligne=n;
	  this.nColonne=m;
	  int k=0;
	  this.board= new Integer[m][n];
	  this.etatInitiale=new Integer[m][n];
	 for(int i=0;i<n;i++)
	    for(int j=0;j<m;j++)
         {
             this.board[j][i]=i+j*this.nColonne+1;	 
	           k=this.board[j][i];
			}
	  this.board[this.nColonne-1][this.nligne-1]=0;
     this.etatInitiale=this.CopyBoard();
	  crdVide[0]=this.nColonne-1;
    crdVide[1]=this.nligne-1;

     this.tqListeners = new ArrayList<>();
}
public int getnblignes()
{
  return this.nligne;
} 

public int getnbcolonnes()
{
  return this.nColonne;
}
public int[] posCaseVide()
{
  return this.crdVide;
}
public void afficherEtat()
{
	 for(int i=0;i<nligne;i++)
	   { for(int j=0;j<nColonne;j++)
	        System.out.print("       "+this.board[j][i]+" ");
	      System.out.println("\n");
		}
	System.out.println("\nCase vide ( "+crdVide[0]+ "," +crdVide[1]+" )");
}

public boolean gauche()
{  Integer a=this.board[crdVide[0]][crdVide[1]];
   if(crdVide[0]!=0)
    {this.board[crdVide[0]][crdVide[1]]= this.board[crdVide[0]-1][crdVide[1]];
    this.board[crdVide[0]-1][crdVide[1]]=a;
    crdVide[0]= crdVide[0]-1;
     return true;
    }
    return false;
}

public boolean droite()
{  Integer a=this.board[crdVide[0]][crdVide[1]];
   if(crdVide[0]!=this.nColonne-1)
    {this.board[crdVide[0]][crdVide[1]]= this.board[crdVide[0]+1][crdVide[1]];
    this.board[crdVide[0]+1][crdVide[1]]=a;
    crdVide[0]= crdVide[0]+1;
     return true;
    }
  return false;
}

public boolean haut()
{  Integer a=this.board[crdVide[0]][crdVide[1]];
   if(crdVide[1]!=0)
    {this.board[crdVide[0]][crdVide[1]]= this.board[crdVide[0]][crdVide[1]-1];
    this.board[crdVide[0]][crdVide[1]-1]=a;
    crdVide[1]= crdVide[1]-1;
     return true;
    }
  return false;
}


public boolean bas()
{  Integer a=this.board[crdVide[0]][crdVide[1]];
   if(crdVide[1]!=this.nligne-1)
    {
      this.board[crdVide[0]][crdVide[1]]= this.board[crdVide[0]][crdVide[1]+1];
      this.board[crdVide[0]][crdVide[1]+1]=a;
      crdVide[1]= crdVide[1]+1;
      return true;
    }
  return false;
}

public boolean deplacer(char c)
{
  Boolean b;
	switch (c) {
		   case 'H' :
            b=this.haut();
            infoEstDeplace();
		      return b;
		   case 'G' :
           b=this.gauche();
           infoEstDeplace();
          return b;
		   case 'D' :
            b=this.droite();
            infoEstDeplace();
          return b;
		   case 'B' : 
            b=this.bas();
            infoEstDeplace();
          return b;
		   
		   default : 
            return false;
		}
}
public void randomGrid() {
  melanger(nColonne * nligne * 100);
}

public void melanger(int n)
{
  this.informer = false;
  char[] direc= {'H','B','G','D'};
  Random r =new Random();
  boolean k;
  int randome;
   for(int j=0;j<n;j++)

   {  do
      {  
         randome=r.nextInt(4);
         k=deplacer(direc[randome]);

       }while(k==false);

  }
  this.informer = true; 

}


public boolean estRanger()
{
    for(int i=0;i<nligne;i++)
	   for(int j=0;j<nColonne;j++)
             if(this.etatInitiale[j][i]!= this.board[j][i])
                return false;
    return true;
    
}

public Integer getSquare(int j, int i) {
  if (j > this.nColonne-1 || i > this.nligne-1 || i < 0 || j < 0) {
    return null;
  }

  return board[j][i];
}


         /**
	* 
	* méthode qui ajoute un écouteur dans la liste des écouteurs.
	*/
@Override
	public void ajouterTaquinListener(TaquinListener listener) {
			tqListeners.add(listener);
	}
         /**
	* 
	* méthode qui supprime un écouteur dans la liste des écouteurs.
	*/
	@Override
	public void supprimerTaquinListener(TaquinListener listener) {
		tqListeners.remove(listener);
	}

	 /**
	* 
	* méthode qui informe l'écouteur qu'un déplacement à été fait.
	*/
  @Override
  public void infoEstDeplace()
  {
    if(!this.informer)
      return;
    for(TaquinListener tq : tqListeners)
      tq.move();
  }
  /**
   * Redessine l'affichage après un mouvement
   * @param abstracTaquin TODO
   */
  
  public void Deplace(AbstracTaquin abstracTaquin) {
  	abstracTaquin.repaint();
  }
  
}