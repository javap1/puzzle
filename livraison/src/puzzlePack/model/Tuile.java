package model;
import java.awt.*;
import javax.swing.*;
public class Tuile
{
   public Tuile()
   {

   }
   public ImageIcon lireImage(String chemin)
   {
    Toolkit tk=Toolkit.getDefaultToolkit();
    Image src1=tk.getImage(chemin);
    return new ImageIcon(src1);
   }
   public ImageIcon lireImageTaille(String chemin,int width,int height )
   {
      Toolkit tk=Toolkit.getDefaultToolkit();
      Image src1=tk.getImage(chemin);
      Image src2=src1.getScaledInstance(width, height, Image.SCALE_DEFAULT);
      return new ImageIcon(src2);

    }
    


}
