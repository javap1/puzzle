package vue.vueConsole;

/**
 * @author Mohamed Akaddar
 * @date 15 mars 2022
 * ConsoleVue pour jouer en mode console.
 */

import controller.TaquinListener;
import java.util.Scanner;
import model.Taquin;

public class ConsoleVue implements TaquinListener {

  private Taquin taquin;

  public ConsoleVue(int w, int h)
  {
    this(new Taquin(w,h));
  }

  public ConsoleVue(Taquin tq)
  {
    this.taquin = tq;
    this.taquin.ajouterTaquinListener(this);
  }


  public void Jeu()
  {
    Scanner sc = new Scanner(System.in);
    //On s'intéresse pas aux événement au moment du mélange
    this.taquin.supprimerTaquinListener(this);

    //On mélange le taquin :
    System.out.println("                  = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =");
    System.out.println("= = = = = = = = = =                        Jeu Taquin                         = = = = = = = = = = = = =");
    System.out.println("                  = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =");

    System.out.println("\nTaquin à résoudre : ");
    this.taquin.melanger(20);
    //Affichage du Taquin
    this.taquin.afficherEtat();
    

    
    //On écoute au événements :
    this.taquin.ajouterTaquinListener(this);

    //Commencons à jouer tant que la partie n'est pas terminé
    int i=0;
    while(!this.taquin.estRanger())
    {
      System.out.println("\n* * * * * * * * * * *  Tour : "+(++i)+" * * * * * * * * * *");
      System.out.println("Donner le prochain déplacement :\n  Haut   : 'H' \n  Bas    : 'B' \n  Gauche : 'G' \n  Droite : 'D' ");
      char c = (char) sc.next().charAt(0);

      while(!(c=='H' || c=='B' || c=='G' || c=='D'  ))
      {
        System.out.println("Mouvement invalide !!!");
        System.out.println("Re-donner le prochain déplacement :\n  Haut   : 'H' \n  Bas    : 'B' \n  Gauche : 'G' \n  Droite : 'D' ");
        c = (char) sc.next().charAt(0);
      }
      
      this.taquin.deplacer(c);
     
        

    }
    System.out.println("Wow Partie terminée !!");
  }

  /**
	 * Fonction qui affiche le Taquin après chaque mouvement
	 */
  @Override
  public void move()
  {
    afficherGrille();
  }

  public void afficherGrille()
  {
    this.taquin.afficherEtat();
  }
}
