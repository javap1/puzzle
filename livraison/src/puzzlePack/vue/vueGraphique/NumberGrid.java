package vue.vueGraphique;
import java.awt.*;
import java.text.BreakIterator;

import javax.swing.*;

import javax.swing.text.AbstractDocument.BranchElement;

import model.Taquin;

public class NumberGrid extends AbstracTaquin{
    public NumberGrid(Taquin taquinGrid) {
		super(taquinGrid);
	}

	/**
	 * Dessin du taquin avec des nombres
	 * @param g l'objet avec lequel on dessine
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		int cellSize = getCellSize();
		int fontSize = getCellSize()/3;
		for(int i = 0 ; i< getTaquinGrid().getnblignes(); i++) {
			for(int j = 0 ; j < getTaquinGrid().getnbcolonnes() ; j++) {
				if(getTaquinGrid().getSquare(j,i) != 0){
					g.setFont(new Font("TimesRoman",Font.BOLD,fontSize));
					FontMetrics fontMetrics = g.getFontMetrics();
					String text = ""+getTaquinGrid().getSquare(j,i);
					g.drawString(text, cellSize*j + (cellSize - (fontMetrics.stringWidth(text)))/2, cellSize*(i+1)-(cellSize/3));
				}
			}
		}

		drawSelectedSquare(g);
		drawGrid(g);
	}

    
}
