package vue.vueGraphique;
import model.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

import controller.TaquinListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.awt.Image;

public class FentrePrincipale extends JFrame implements TaquinListener{
        JPanel panelprincipal=new JPanel();
        JPanel panelhaut=new JPanel();
        JPanel panelcentre=new JPanel();
        JPanel panelbas=new JPanel();
        JPanel paneldroit=new JPanel();
        JButton buttoncharger=new JButton("Charger");
        JButton buttondecouper=new JButton("Découper");
        JButton buttonMelanger=new JButton("Mélanger");
        JTextField textchemin=new JTextField("le chemin de l'image",20);
        JTextField textnblignes=new JTextField();
        JTextField textnbcolonnes=new JTextField();
        JLabel labelchemin =new JLabel("Chemin",JLabel.LEFT);
        JLabel Labelvide =new JLabel(" ");
        JLabel labellingne=new JLabel("Lignes");
        JLabel labelicone=new JLabel();
        private Taquin taquin;
        private ImageGrid grid;
        private Dialogue d;
        private static final String NUMBER_GRID = "NUMBER";
	    private static final String IMAGE_GRID = "IMAGE";

	    private static final int DEFAULT_NBCOL = 3;
	    private static final int DEFAULT_NBLI = 3;

	    private ImageGrid imageGrid;
	    private NumberGrid numberGrid;
        public FentrePrincipale()
        {
            //ajoutComposant();
            createMenuBar();
            createMainUI();
        }
        public FentrePrincipale(int w,int h,String name)
        {
                setSize(w, h);
                setTitle(name);
                pack();
                setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                setLocationRelativeTo(null);
                setResizable(false);
                createMenuBar();
                //createMainUI();
                //ajoutComposant();
                setVisible(true);
        }
        private void createMenuBar() 
	{
		JMenuBar menuBar = new JMenuBar();

		JMenu menuFichier = new JMenu("Jeu");
		JMenu menuAffichage = new JMenu("Mode");

		JMenuItem itemNewGame = new JMenuItem("Nouvelle partie");
		menuFichier.add(itemNewGame);

		//Evenement de l'item "nouvelle partie"
		itemNewGame.addActionListener((ActionEvent e) -> {
			Dialogue dialog = new Dialogue(this);
			Integer taquinGridWidth = dialog.getSelectedWidth();
			Integer taquinGridHeight = dialog.getSelectedHeight();

			//On vérifie les tailles rentrées
			if(taquinGridWidth != null && taquinGridHeight != null) {
				if(taquinGridWidth >=2 && taquinGridWidth <22 && taquinGridHeight >=2 && taquinGridHeight <22) {
					newGame(taquinGridWidth, taquinGridHeight);
				} else{
					JOptionPane.showMessageDialog(this, "vérifier La largeur et la hauteur de la grille ", "Erreur", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		//Création de la sous-catégorie "Mode image" de "Mode"
		JCheckBoxMenuItem itemGridType = new JCheckBoxMenuItem("Mode image");
		itemGridType.setState(true);
		menuAffichage.add(itemGridType);
		//Evenement de l'item "mode image"
		itemGridType.addItemListener((ItemEvent e) -> {
			if(itemGridType.getState())
				showTaquinGrid(IMAGE_GRID);
			else
				showTaquinGrid(NUMBER_GRID);
		});
		JMenuItem itemChangerImage = new JMenuItem("Changer l'image");
		menuAffichage.add(itemChangerImage);
		//Evenement de l'item "mode chiffres"
		itemChangerImage.addActionListener((ActionEvent e) -> {
            // Afficher une boîte de dialogue à chaque fois qu'on clique sur le bouton
                    //JOptionPane.showMessageDialog(null, "Merci d'avoir cliqué");
            /*        JFileChooser file=new JFileChooser();
                    FileNameExtensionFilter filtrer =new FileNameExtensionFilter("IMAGE","png","jpg","gif");
                    file.addChoosableFileFilter(filtrer);
                    int result= file.showSaveDialog(null); 

                    if(result== JFileChooser.APPROVE_OPTION)
                    {
                        File imageSelected=file.getSelectedFile();
                        String path=imageSelected.getAbsolutePath();
                        textchemin.setText(path);
                        ImageIcon myimage=new ImageIcon(path);  
                        Image img=myimage.getImage();
                        Image newimage=img.getScaledInstance(labelicone.getWidth(), labelicone.getHeight(),java.awt.Image.SCALE_SMOOTH );
                        imageGrid.setImage(newimage);

                    }*/
			//Liste des extensions de fichier autorisées

			FileNameExtensionFilter imagesFilter = new FileNameExtensionFilter("Fichiers image", "png", "bmp", "jpg", "jpeg");

			//Choix d'une image dans les dossiers de l'utilisateur
			JFileChooser imageChoosed = new JFileChooser();
			imageChoosed.setDialogTitle("Choisir une image");
			imageChoosed.setAcceptAllFileFilterUsed(false);
			imageChoosed.setFileFilter(imagesFilter);

			int returnVal = imageChoosed.showOpenDialog(FentrePrincipale.this);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				//On essaye de lire le fichier choisi par l'utilisateur
				try {
					Image image = ImageIO.read(new File(imageChoosed.getSelectedFile().getPath()));
					imageGrid.setImage(image);
				} catch (IOException ex) {
					//Renvoie un message d'erreur si le fichier n'est pas valide
					JOptionPane.showMessageDialog(FentrePrincipale.this, "Impossible d'ouvrir le fichier", "Erreur", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		//Ajout des catégories à la barre de navigation
		menuBar.add(menuFichier);
		menuBar.add(menuAffichage);

		setJMenuBar(menuBar);
	}
    /**
	 * Création du contenu de la fenêtre de jeu
	 */
	private void createMainUI() {
		setLayout(new CardLayout());
		newGame(DEFAULT_NBCOL, DEFAULT_NBLI);

		this.add(imageGrid, IMAGE_GRID);
		this.add(numberGrid, NUMBER_GRID);
       // showTaquinGrid(NUMBER_GRID);
		showTaquinGrid(IMAGE_GRID);
	}

	/**
	 * Lancement d'une nouvelle partie
	 * @param w la largeur de la grille du taquin
	 * @param h la hauteur de la grille du taquin
	 */
	protected void newGame(int w, int h) {
		this.taquin = new Taquin(w, h);
		this.taquin.ajouterTaquinListener(this);

		//Création du mode "Nombre" s'il existe pas
		if (numberGrid == null) {
			numberGrid = new NumberGrid(this.taquin);

		} else {
			numberGrid.setTaquinGrid(this.taquin);
		}
		if (imageGrid == null) {
			ImageIcon myimage=new ImageIcon("../../../livraison/res/default.jpg");  
            Image img=myimage.getImage();
            imageGrid = new ImageGrid(this.taquin,img);
		} else {
			imageGrid.setTaquinGrid(this.taquin);
		}

		imageGrid.repaint();
		numberGrid.repaint();
	}

	/**
	 * Affichage de la bonne grille en fonction qu'il s'agisse du mode "Image" ou "Nombre"
	 * @param gridType le type d'affichage
	 */
	private void showTaquinGrid(String gridType) {
		CardLayout cl = (CardLayout)(getContentPane().getLayout());
		cl.show(getContentPane(), gridType);

		if(gridType == NUMBER_GRID) {
			numberGrid.requestFocus();
		} else if(gridType == IMAGE_GRID) {
			imageGrid.requestFocus();
		}
	}
    public void gameOver()
    {
       
    }
	/**
	 * Permet d'afficher une fenêtre de victoire au moment où le joueur à gagné
	 */
	@Override
	public void move() {
        if (this.taquin.estRanger() == true){
			imageGrid.repaint();
			numberGrid.repaint();

			int answer = JOptionPane.showConfirmDialog(this,"Vous avez gagné ! Voulez vous rejouer ?", "VICTOIRE !", JOptionPane.YES_NO_OPTION);
			if(answer == JOptionPane.YES_OPTION) {
				this.taquin.randomGrid();
			}
		}
	}

        public void ajoutComposant()
        {
            
            this.setLayout(new BorderLayout());
            this.setPreferredSize(new Dimension(1300,1000));

            panelprincipal.setLayout(new BorderLayout());
            panelprincipal.setPreferredSize(new Dimension(500,200));
            

            panelhaut.setLayout(new FlowLayout());
            panelhaut.setBackground(Color.magenta);
            panelhaut.setPreferredSize(new Dimension(0,60));
            
            //labelchemin.setText("Chemin");
            //panelhaut.add(labelchemin,FlowLayout.LEFT);
            //panelhaut.add(textchemin,FlowLayout.CENTER);
            //panelhaut.add(buttoncharger,FlowLayout.RIGHT);
            //panelprincipal.add(panelhaut);

            panelbas.setLayout(new FlowLayout());
            panelbas.setBackground(Color.magenta);
            panelbas.setPreferredSize(new Dimension(0,60));

            //panelgauche.add(buttoncharger);
            panelbas.add(buttondecouper,FlowLayout.LEFT);
            panelbas.add(Labelvide,FlowLayout.CENTER);
            panelbas.add(buttonMelanger,FlowLayout.RIGHT);



            panelcentre.setBackground(Color.magenta);
            panelcentre.setPreferredSize(new Dimension(0,0));
            //panelcentre.add(labelchemin);

            paneldroit.setBackground(Color.magenta);
            paneldroit.setPreferredSize(new Dimension(0,60));

            panelprincipal.setLayout(new BorderLayout());
            panelprincipal.setPreferredSize(new Dimension(500,800));
            panelprincipal.setBackground(Color.WHITE);
            panelprincipal.add(labelicone,BorderLayout.CENTER);

            ///panelprincipal.add(panelgauche,BorderLayout.NORTH);
           // panelprincipal.add(panelhaut,BorderLayout.SOUTH);
            /*taquin=new Taquin(3,3);
            ImageIcon myimage=new ImageIcon("/home/kissami211/Téléchargements/akaddar-kissami-benfaras-khalile/livraison/res/photo1.jpg");  
            Image img=myimage.getImage();
             grid=new ImageGrid(taquin,img);*/
            // d=new Dialogue(this);
           /* if (grid == null) {
                grid = new NumberGrid(this.taquin);
            } else {
                grid.setTaquinGrid(this.taquin);
            }*/
             //this.add(grid);
             //grid.infoEstDeplace();
             //this.add(d);
             //this.add(grid,BorderLayout.CENTER);
             //this.add(panelbas,BorderLayout.SOUTH);
             
            //this.getContentPane().add(panelgauche,BorderLayout.SOUTH);
            /*buttoncharger.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt)
                {
                    // Afficher une boîte de dialogue à chaque fois qu'on clique sur le bouton
                    //JOptionPane.showMessageDialog(null, "Merci d'avoir cliqué");
                    JFileChooser file=new JFileChooser();
                    FileNameExtensionFilter filtrer =new FileNameExtensionFilter("IMAGE","png","jpg","gif");
                    file.addChoosableFileFilter(filtrer);
                    int result= file.showSaveDialog(null); 

                    if(result== JFileChooser.APPROVE_OPTION)
                    {
                        File imageSelected=file.getSelectedFile();
                        String path=imageSelected.getAbsolutePath();
                        textchemin.setText(path);
                        ImageIcon myimage=new ImageIcon(path);  
                        Image img=myimage.getImage();
                        Image newimage=img.getScaledInstance(labelicone.getWidth(), labelicone.getHeight(),java.awt.Image.SCALE_SMOOTH );
                        ImageIcon finalimage=new ImageIcon(newimage);
                        labelicone.setIcon(finalimage);

                    }
                }   
            });*/
            /*buttonMelanger.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt)
                {
                    // Afficher une boîte de dialogue à chaque fois qu'on clique sur le bouton
                    JOptionPane.showMessageDialog(null, "Merci d'avoir cliqué");
                    
                }   
            });*/
           /* buttondecouper.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent evt)
                {
                    // Afficher une boîte de dialogue à chaque fois qu'on clique sur le bouton
                    //JOptionPane.showMessageDialog(null, "découper");
                    /*taquin=new Taquin(3, 3);
                    grid=new NumberGrid(taquin);
                    add(grid);
                    grid.infoEstDeplace();


                    
                }   
            });*/
        }
    public static void main (String [] aStrings)
    {
        //FentrePrincipale window = new FentrePrincipale(800,800,"Taquin game");
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                
                FentrePrincipale f=new FentrePrincipale();
                f.setSize(500000, 2000);
                f.setTitle("Taquin");
                f.gameOver();
                f.pack();
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                f.setLocationRelativeTo(null);
                //f.setResizable(false);
                f.setVisible(true);



            }
        });
    }
    
}
